import * as fs from 'fs';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { Command } from 'commander';

const command = new Command();
command
  .name('single-app-swagger-json-generator')
  .arguments('--app-path src --app-version 1')
  .option('-m, --module-name app', 'module name')
  .option('-n, --app-name pmp', "app's name")
  .option('-o, --output-dir reference', 'output directory to write the swagger spec to');
command.parse(process.argv);

const outputDir = `${process.cwd()}/reference`;
const importPath = `${process.cwd()}/src/modules/app/app.module.ts`;

(async () => {
  try {
    const importedObject = await import(importPath);
    const appModule = importedObject.AppModule;
    const swaggerBaseConfig = new DocumentBuilder()
      .setTitle('PMP API')
      .setDescription('The PMP API description')
      .setVersion('1.0')
      .addTag('pmp')
      .setVersion(command.appVersion)
      .build();

    const app = await NestFactory.create(appModule);
    const document: OpenAPIObject = SwaggerModule.createDocument(app, swaggerBaseConfig);
    fs.writeFileSync(`${outputDir}/api.json`, JSON.stringify(document));
  } catch (error) {
    console.error('Error : ', error);
    throw error;
  }
})();
