# Preference Management Platform API

Lambda function that hosts the REST API of the PMP project.

## Getting Started

### Manual setup

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies
    
    ```
    cd path/to/repo; npm install
    ```

3. Start the function locally
    
    ```
    npm start
    ```

4. Requests are going to be served at http://localhost:3000/
    

## Testing and coverage

Simply run `npm test` and all your tests in the `test/` directory will be run.

You can generate a code coverage report from the tests by running `npm run coverage`. The report will be in the `coverage/` directory.

The code coverage is automatically run as part of the build process on Gitlab CI and the build will fail if the coverage is below these thresholds:

| **Lines** | **Functions** | **Branches** |
| ----- | --------- | -------- |
| 100%   | 100%       | 100%      |

Make sure that your code is correctly covered **before** pushing it.

### E2E tests

In order to run e2e tests you need to have a test database running.
The easiest way to do that is to have the project running in docker (`docker-compose up`) and then using


```
docker-compose exec api sh
```

to get into the container. After that use

```
npm run e2e:test
```

Alternatively, if you want to run the tests on your local machine, you have to change the `host` 
in the database config to `0.0.0.0`.


## Linting

We follow the [JavaScript guidelines from Airbnb](https://github.com/airbnb/javascript).

Run `npm run lint` to check if the code is compliant with the guidelines. You can run `npm run lint-fix` to apply the standard fixes (spaces, tabs, etc.) automatically.

A linting check is automatically run as part of the build process on Gitlab CI and the build will fail if there is any linting error. Make sure that your code is compliant **before** pushing it.

## Docker

In order to run the project with docker, you need to have access to GitLab's private repositories inside the container. This is done in 2 steps

#### 1. Create a `.npmrc` file and add the following line:

```
//registry.npmjs.org/:_authToken=<AUTH_TOKEN>
```

Replace `<AUTH_TOKEN>` with your token

If you don't have a token, you can create one by following the [guide](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) on GitLab.

#### 2. Create a `.ssh` folder and add a `id_ed25519` file containing your private key, the name of the file and the type of the key has to be exact.

The contents of the file looks something like this:

```
-----BEGIN OPENSSH PRIVATE KEY-----
qweNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACDmCjoeIxDmX880wHLWSfIxvN0YJ0Xb/RERenmdKkAshgAAAJDtCA0G7QgN
BgAAAAtzc2gtZWQyNTUxOQfasfsa880wHLWSfIxvN0YJ0Xb/RERenmdKkAshg
AAAECdu2tKBvhdcY2n/jKzkasfasfYmfM0MOsaz9IOYKOh4qwetZJ8jG8
3RgnRdv9ERqweqweAABmdaQFBgc=
-----END OPENSSH PRIVATE KEY-----
```


If you don't have an SSH key, you can create one by following the [guide](https://docs.gitlab.com/ee/ssh/README.html) on GitLab.

Remember to never commit and upload your tokens to gitlab, if this happens, invalidate them and create new ones.

After that simply run

```
docker-compose build
``` 

to build the project and

```
docker-compose up
```

to start the project

If you want to run the service in an interactive shell, use

```
docker-compose exec <service> sh
```

## Database and REST

We use Postgres as our database, TypeORM as our ORM and [nestjsx/crud](https://github.com/nestjsx/crud) for generating CRUD endpoints and CRUD database operations.

### Adding new entities and endpoints

Adding new entities is simple. We always extend from the `BaseModel` in order to get the default properties, such as `created_at` and `updated_at`.
We name the file with the `.model.ts` extension, e.g. `preference.model.ts` and the class without the extension e.g. `Preference`.

The `BaseModel` does not follow this convention as to not get picked up by TypeOrm, it's just an abstract model.

We use the `@ApiProperty` to document our columns with swagger. For more information on how to document the models read [this guide](https://docs.nestjs.com/openapi/introduction).
```ts
import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, OneToMany } from 'typeorm';
import { BaseModel } from './base-model';
import { PreferenceValue } from './preference-value.model';

@Entity()
export class Preference extends BaseModel {
  @ApiProperty({ required: true, description: 'The name of the preference' })
  @Column({ type: 'varchar', nullable: false })
  name: string;

  @ApiProperty({ type: () => PreferenceValue })
  @OneToMany(() => PreferenceValue, (value) => value.preference, {})
  preference_values: PreferenceValue[];
}

```

We use `DTOs` to document the data accepted by our API. The helper classes such as `PickType` and `OmitType` help us pick what properties are allowed from our models.
For more information read [this guide](https://docs.nestjs.com/openapi/mapped-types).

```ts
import { PickType } from '@nestjs/swagger';
import { Preference } from '../../database/models/preference.model';

export class CreatePreferenceDto extends PickType(Preference, ['name']) {}
```

This `DTO` will allow only a `name` property to be passed in our body.

To add a new controller, we implement the `CrudController<T>` interface and use the `@Crud()` decorator.

```ts
@Crud({
  model: {
    type: Preference,
  },
})
@ApiTags('preferences')
@Controller('preferences')
export class PreferencesController implements CrudController<Preference> {
  constructor(public service: PreferencesService) {}
}
```

This will create a controller for the `Preference` model. It will generate the following endpoints:

```
GET /preferences/:id - Get one preference
GET /preferences - Get many preferences
POST /preferences - Create one preference
POST /preferences/bulk - Create many preferences
PATCH /preferences/:id - Update one preference
PUT /preferences/:id - Replace one preference
DELETE /preferences/:id - Delete one preference
```

For more information on how to customize this behavior, read [this guide](https://github.com/nestjsx/crud/wiki/Controllers#description)

### Migrations

In order to generate a migration you need to have a running database and be inside the docker container.
After that, use

```
npm run migration:generate <migration-name>
```
This command will check the database for any model changes against the database and will automatically create the queries required to update the database.
It generates a class with `up` and `down` methods, the `up` method will execute the queries that update the table with the new changes.
The `down` method is used to rollback the migration if something goes wrong.

We execute migrations automatically before deployment, if the migration is unsuccessful the deployment is stopped.

If you want to run the migration manually, you can use

```
npm run migration:run
```

If you want to revert the latest migration, you can use

```
npm run migration:revert
```

This command will execute `down` in the latest executed migration. If you need to revert multiple migrations you must call this command multiple times.


If you want to run migrations outside docker, you can change the host in `database.config.ts` to `0.0.0.0`. Keep in mind you still need the postgres database running inside docker.

For more information and rules about how to run safe migrations, you can read [this guide](https://gitlab.com/didomi/start/-/blob/master/docs/DATABASES.md#rules-for-safe-migrations).
