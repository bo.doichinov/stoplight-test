{
  "openapi": "3.0.0",
  "info": {
    "title": "PMP API",
    "description": "The PMP API description",
    "version": "1.0",
    "contact": {}
  },
  "tags": [
    {
      "name": "pmp",
      "description": ""
    }
  ],
  "servers": [
    {
      "url": "http://localhost:3000",
      "description": "local"
    },
    {
      "url": "https://api-staging.didomi.io",
      "description": "staging"
    }
  ],
  "components": {
    "schemas": {
      "GetManyPreferenceValueResponseDto": {
        "type": "object",
        "properties": {
          "data": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/PreferenceValue"
            }
          },
          "count": {
            "type": "number"
          },
          "total": {
            "type": "number"
          },
          "page": {
            "type": "number"
          },
          "pageCount": {
            "type": "number"
          }
        },
        "required": [
          "data",
          "count",
          "total",
          "page",
          "pageCount"
        ]
      },
      "PreferenceValue": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "A unique, randomly generated ID"
          },
          "created_at": {
            "type": "string",
            "description": "Creation date of the entity (ISO8601)",
            "example": "2021-07-16T09:36:20.233Z"
          },
          "updated_at": {
            "type": "string",
            "description": "Update date of the entity (ISO8601)",
            "example": "2021-07-16T09:36:20.233Z"
          },
          "name": {
            "type": "string"
          }
        },
        "required": [
          "id",
          "created_at",
          "updated_at",
          "name"
        ]
      },
      "CreateManyPreferenceValueDto": {
        "type": "object",
        "properties": {
          "bulk": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/PreferenceValue"
            }
          }
        },
        "required": [
          "bulk"
        ]
      },
      "Preference": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "A unique, randomly generated ID"
          },
          "created_at": {
            "type": "string",
            "description": "Creation date of the entity (ISO8601)",
            "example": "2021-07-16T09:36:20.233Z"
          },
          "updated_at": {
            "type": "string",
            "description": "Update date of the entity (ISO8601)",
            "example": "2021-07-16T09:36:20.233Z"
          },
          "name": {
            "type": "string",
            "description": "The name of the preference"
          },
          "description": {
            "type": "string",
            "description": "The description of the preference"
          },
          "organization_id": {
            "type": "string",
            "description": "The organization id of the preference"
          }
        },
        "required": [
          "id",
          "created_at",
          "updated_at",
          "name",
          "description",
          "organization_id"
        ]
      },
      "CreatePreferenceDto": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "description": {
            "type": "string"
          },
          "organization_id": {
            "type": "string"
          }
        },
        "required": [
          "name",
          "description",
          "organization_id"
        ]
      },
      "UpdatePreferenceDto": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "description": {
            "type": "string"
          },
          "organization_id": {
            "type": "string"
          }
        },
        "required": [
          "name",
          "description",
          "organization_id"
        ]
      },
      "PatchPreferenceDto": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "description": {
            "type": "string"
          },
          "organization_id": {
            "type": "string"
          }
        }
      }
    }
  },
  "paths": {
    "/preferences/{preference_id}/preference-values/{id}": {
      "get": {
        "operationId": "getOneBasePreferenceValuesControllerPreferenceValue",
        "summary": "Retrieve one PreferenceValue",
        "parameters": [
          {
            "name": "preference_id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "fields",
            "description": "Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": false
          },
          {
            "name": "join",
            "description": "Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": true
          },
          {
            "name": "cache",
            "description": "Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "integer",
              "minimum": 0,
              "maximum": 1
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Get one base response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PreferenceValue"
                }
              }
            }
          }
        },
        "tags": [
          "Consents and Preferences"
        ]
      },
      "patch": {
        "operationId": "updateOneBasePreferenceValuesControllerPreferenceValue",
        "summary": "Update one PreferenceValue",
        "parameters": [
          {
            "name": "preference_id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "number"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/PreferenceValue"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PreferenceValue"
                }
              }
            }
          }
        },
        "tags": [
          "preference-values"
        ]
      },
      "delete": {
        "operationId": "deleteOneBasePreferenceValuesControllerPreferenceValue",
        "summary": "Delete one PreferenceValue",
        "parameters": [
          {
            "name": "preference_id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "number"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Delete one base response"
          }
        },
        "tags": [
          "preference-values"
        ]
      }
    },
    "/preferences/{preference_id}/preference-values": {
      "get": {
        "operationId": "getManyBasePreferenceValuesControllerPreferenceValue",
        "summary": "Retrieve many PreferenceValue",
        "parameters": [
          {
            "name": "preference_id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "fields",
            "description": "Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": false
          },
          {
            "name": "s",
            "description": "Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "filter",
            "description": "Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": true
          },
          {
            "name": "or",
            "description": "Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": true
          },
          {
            "name": "sort",
            "description": "Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": true
          },
          {
            "name": "join",
            "description": "Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "style": "form",
            "explode": true
          },
          {
            "name": "limit",
            "description": "Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "offset",
            "description": "Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "page",
            "description": "Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "cache",
            "description": "Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>",
            "required": false,
            "in": "query",
            "schema": {
              "type": "integer",
              "minimum": 0,
              "maximum": 1
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Get many base response",
            "content": {
              "application/json": {
                "schema": {
                  "oneOf": [
                    {
                      "$ref": "#/components/schemas/GetManyPreferenceValueResponseDto"
                    },
                    {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/PreferenceValue"
                      }
                    }
                  ]
                }
              }
            }
          }
        },
        "tags": [
          "preference-values"
        ]
      },
      "post": {
        "operationId": "createOneBasePreferenceValuesControllerPreferenceValue",
        "summary": "Create one PreferenceValue",
        "parameters": [
          {
            "name": "preference_id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/PreferenceValue"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Get create one base response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PreferenceValue"
                }
              }
            }
          }
        },
        "tags": [
          "preference-values"
        ]
      }
    },
    "/preferences/{preference_id}/preference-values/bulk": {
      "post": {
        "operationId": "createManyBasePreferenceValuesControllerPreferenceValue",
        "summary": "Create many PreferenceValue",
        "parameters": [
          {
            "name": "preference_id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreateManyPreferenceValueDto"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Get create many base response",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/PreferenceValue"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "preference-values"
        ]
      }
    },
    "/preferences": {
      "get": {
        "operationId": "PreferencesController_findAllByOrg",
        "summary": "Retrieve a list of preferences by organization",
        "parameters": [
          {
            "name": "organization_id",
            "required": true,
            "in": "query",
            "description": "The ID of the organization for which to retrieve preferences",
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "$limit",
            "required": false,
            "in": "query",
            "description": "Number of results to return",
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "$skip",
            "required": false,
            "in": "query",
            "description": "Number of results to skip",
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "$sort",
            "required": false,
            "in": "query",
            "description": "Property to sort results",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A list of Preference objects",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Preference"
                  }
                }
              }
            }
          }
        },
        "tags": [
          "preferences"
        ]
      },
      "post": {
        "operationId": "PreferencesController_createOne",
        "summary": "Create a preference",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreatePreferenceDto"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "The created Preference object",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Preference"
                }
              }
            }
          }
        },
        "tags": [
          "preferences"
        ]
      }
    },
    "/preferences/{id}": {
      "get": {
        "operationId": "PreferencesController_findOne",
        "summary": "Retrieve a preference",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A Preference object",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Preference"
                }
              }
            }
          }
        },
        "tags": [
          "Consents and Preferences"
        ]
      },
      "put": {
        "operationId": "PreferencesController_updateOne",
        "summary": "Update a preference",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UpdatePreferenceDto"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "The updated Preference object",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Preference"
                }
              }
            }
          }
        },
        "tags": [
          "preferences"
        ]
      },
      "patch": {
        "operationId": "PreferencesController_patchOne",
        "summary": "Patch a preference",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/PatchPreferenceDto"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "The patched Preference object",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Preference"
                }
              }
            }
          }
        },
        "tags": [
          "preferences"
        ]
      },
      "delete": {
        "operationId": "PreferencesController_deleteOne",
        "summary": "Delete a preference",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The deleted Preference object",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Preference"
                }
              }
            }
          }
        },
        "tags": [
          "preferences"
        ]
      }
    }
  }
}
